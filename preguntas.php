<?php 
    require('conexion.php');

    
    
?> 

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Offcanvas template for Bootstrap</title>
    <link href="bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="preguntas.css" rel="stylesheet">
</head>

<body class="bg-light">
    <!-- esto es para el navbar -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
        <a class="navbar-brand mr-auto mr-lg-0" href="#">PREGUNTAS DEL CURSO</a>
        <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="admin.html">Ir inicio administrador</a>
                </li>
        </div>
    </nav>
    <!-- esto es para lo del medio "MAIN" -->
    <main role="main" class="container">
        <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
            <div class="lh-100">
                <h6 class="mb-0 text-white lh-100">Seleccione el tipo de Pregunta</h6>
            </div>
            <div style="margin-left: 350px">
                <button class="btn btn-danger" data-target="#seleccion_multiple" data-toggle="modal">Seleccion Multiple</button>
                <button class="btn btn-danger" data-target="#falso_verdadero" data-toggle="modal">Falso/Verdadero</button>
                <button class="btn btn-danger" data-target="#rellenar_espacios" data-toggle="modal">Rellenado de Espacios</button>
            </div>
        </div>
        <!-- aca pone la respuesta mas la pregunta -->
        <div class="my-3 p-3 bg-white rounded box-shadow">
                    <?php 
                            $consultar_preguntas=mysqli_query($conexion,"SELECT * FROM pregunta p 
                            INNER JOIN respuesta r ON (p.id_pregunta=r.id_pregunta)");                            
                            while($f = mysqli_fetch_array($consultar_preguntas)){                                                        
                    ?> 
            <div class="media text-muted pt-3">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                                   
                    <strong class="d-block text-gray-dark"><?php echo $f['nombre_pregunta']; ?></strong>           
                    respuesta: <?php echo $f['respuesta_nombre'];?>                                                
                </p>
            </div>
                    <?php                        
                            }
                    ?>            
        </div>
        <!-- fin respuesta mas pregunta -->
    </main>

    <!-- inicio popup seleccion multiple-->
    <form action="guardar_seleccion_multiple_pregunta.php" method="POST">
        <div class="modal fade bd-example-modal-lg" id="seleccion_multiple" tabindex="-1" role="dialog" aria-labelledby="seleccion_multipleTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="seleccion_multipleTitle">Pregunta de Seleccion Multiple</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- LA PREGUNTA -->
                        <div class="form-group borde">
                            <!-- Esquema para preguntas -->
                            <input type="text" placeholder="Pregunta" class="form-control" name="pregunta_seleccion" style="margin-bottom: 20px" required>
                            <!-- seleccion multiple -->
                            <div>
                                <p class="lead">Seleccione las respuestas que esten corectas</p>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>
                                        Respuesta 1
                                        <input type="radio" name="respuesta1" value="1">
                                        <input type="radio" name="respuesta1" value="0" style="visibility: hidden;" checked>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" placeholder="Respuesta 1" class="form-control" name="res1" required>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        Respuesta 2
                                        <input type="radio" name="respuesta2" value="1">
                                        <input type="radio" name="respuesta2" value="0" style="visibility: hidden;" checked>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" placeholder="Respuesta 2" class="form-control" name="res2" required>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        Respuesta 3
                                        <input type="radio" name="respuesta3" value="1">
                                        <input type="radio" name="respuesta3" value="0" style="visibility: hidden;" checked>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" placeholder="Respuesta 3" class="form-control" name="res3" required>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        Respuesta 4
                                        <input type="radio" name="respuesta4" value="1">
                                        <input type="radio" name="respuesta4" value="0" style="visibility: hidden;" checked>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" placeholder="Respuesta 4" class="form-control" name="res4" required>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        Respuesta 5
                                        <input type="radio" name="respuesta5" value="1">
                                        <input type="radio" name="respuesta5" value="0" style="visibility: hidden;" checked>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" placeholder="Respuesta 5" class="form-control" name="res5" required>
                                </div>
                            </div>
                        </div>
                        <!-- Final Pregunta -->
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="Guardar" class="btn btn-success">
                        <!-- <button type="button" class="btn btn-primary">Guardar</button> -->
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- fin Popup -->
    <!-- Popup Falso Verdadero -->
    <form action="guardar_f_v_pregunta.php" method="POST">
        <div class="modal fade bd-example-modal-lg" id="falso_verdadero" tabindex="-1" role="dialog" aria-labelledby="falso_verdaderoTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="falso_verdaderoTitle">Pregunta Falso/Verdadero</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- LA PREGUNTA -->
                        <div class="form-group borde">
                            <input type="text" placeholder="Pregunta" class="form-control" name="pregunta_f_v" style="margin-bottom: 20px" required>
                            <div>
                                <p class="lead">Seleccione cual es la respuesta correcta</p>
                            </div>
                            <div class="row" id="falso_verdadero">
                                <div class="col-md-6">
                                    <label>
                                        <input type="radio" name="opcion" value="1" checked> Verdadero
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <input type="radio" name="opcion" value="0"> Falso
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- Final Pregunta -->
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="Guardar" class="btn btn-primary">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- fin popup -->
    <!-- Popup rellenar espacios -->
    <form action="guardar_r_espacios.php" method="POST">
        <div class="modal fade bd-example-modal-lg" id="rellenar_espacios" tabindex="-1" role="dialog" aria-labelledby="rellenar_espaciosTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="rellenar_espaciosTitle">Rellenar espacios en blanco</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- LA PREGUNTA -->
                        <div class="form-group borde">
                            <div>
                                <p class="lead">Solo poner 3 puntos para el reconocimiento de la palabra faltante</p>
                            </div>
                            <textarea required name="rellenar_espaios_pregunta" id="mnj" cols="30" rows="10" placeholder="Ejemplo: Mi casa se encuentra en la ... de ..."
                                class="form-control"></textarea>
                            <div>
                                <p class="lead">Colocar las respuestas por orden y separadas por una coma ","</p>
                            </div>
                            <textarea required name="respuestas_rellenar" id="mnj" cols="30" rows="10" placeholder="Ejm: Respuesta1,Respuesta2" class="form-control"></textarea>
                        </div>
                        <!-- Final Pregunta -->
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="Guardar" class="btn btn-primary">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>   

    <!-- fin popup -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
    <script src="bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
</body>

</html>