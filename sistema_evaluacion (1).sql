-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-05-2018 a las 11:49:14
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistema_evaluacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_admin` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `apellido_p` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `apellido_m` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `correo` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_admin`, `nombre`, `apellido_p`, `apellido_m`, `correo`, `password`) VALUES
(1, 'luis', 'arevalo', 'antezana', 'luigui96@hotmail.es', '12345');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id_curso` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL,
  `imagen` text CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `intro_curso` varchar(70) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `temario` text CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `tiempo` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id_curso`, `nombre`, `imagen`, `intro_curso`, `temario`, `tiempo`) VALUES
(1, 'Curso PHP', 'http://localhost/sistema_evaluacion/imagenes/php.png', 'Este es un curso de PHP intermedio', '-tipos de variables\r\n-programacion orientada al objeto\r\n-manejo de fechas\r\n-manejo de memoria cache', '01:00:00'),
(2, 'Curso JavaScript', 'http://localhost/sistema_evaluacion/imagenes/javascript.jpg', 'Es un curso de Javascirpt basico', '-aplicacion de variable', '01:00:00'),
(3, 'Curso CSS', 'http://localhost/sistema_evaluacion/imagenes/css.png', 'Curso de CSS', '-bootstrap\r\n-styles', '01:00:00'),
(4, 'Java', 'http://localhost/sistema_evaluacion/imagenes/java.png', 'Este es un curso de Java para principiantes', '-hola mundo\r\n-poliformismo\r\n-herencia\r\n-elementos de la programacion\r\n-fundamentos de la programacion', '01:00:00'),
(6, 'Tecnicas de produccion de plantas', 'http://localhost/sistema_evaluacion/imagenes/planta.jpg', 'Como plantar un gajo', '-almacigueras\r\n-repique\r\n-seleccion y manejo', '01:00:00'),
(7, 'Tecnicas de produccion de plantas', 'http://localhost/sistema_evaluacion/imagenes/planta.jpg', 'Como plantar un gajo', '-almacigueras\r\n-repique\r\n-seleccion y manejo', '01:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante`
--

CREATE TABLE `estudiante` (
  `id_estudiante` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_p` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `apellido_m` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `carrera` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `correo` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estudiante`
--

INSERT INTO `estudiante` (`id_estudiante`, `nombre`, `apellido_p`, `apellido_m`, `carrera`, `correo`, `password`) VALUES
(1, 'juan', 'martinez', 'prado', 'ing. sistemas', 'lala@gmail.com', '12345'),
(2, 'pedro', 'lara', 'mamani', 'ing_sistemas', 'pedro@gmail.com', '12345'),
(3, 'pepe', 'fernandez', 'martinez', 'publicidad', 'pepe@gmail.com', '12345'),
(4, 'raysa', 'arevalo', 'antezana', 'ing industrial', 'raysa@gmail.com', '12345'),
(5, 'elina', 'guzman', 'biz', 'ing industrial', 'elina@hotmail.com', '12345');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta`
--

CREATE TABLE `pregunta` (
  `id_pregunta` int(11) NOT NULL,
  `nombre_pregunta` varchar(200) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL,
  `id_tipo_pregunta` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pregunta`
--

INSERT INTO `pregunta` (`id_pregunta`, `nombre_pregunta`, `id_tipo_pregunta`, `id_curso`) VALUES
(1, 'El metodo GET sirve para enviar variables?', 2, 1),
(2, 'cuales de estas IDE funcionan con java', 1, 4),
(3, 'java es un lenguaje de etiquetado?', 2, 4),
(4, 'Java es orientado al ... ', 3, 4),
(5, 'java es un lenguaje de programacion orientado al ...', 3, 4),
(10, 'Cuales de estos metodos son verdaderos', 1, 4),
(11, 'Java no es un lenguaje', 2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuesta`
--

CREATE TABLE `respuesta` (
  `id_respuesta` int(11) NOT NULL,
  `respuesta_nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL,
  `es_respuesta` tinyint(1) DEFAULT NULL,
  `id_pregunta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `respuesta`
--

INSERT INTO `respuesta` (`id_respuesta`, `respuesta_nombre`, `es_respuesta`, `id_pregunta`) VALUES
(1, 'verdadero', 1, 1),
(2, 'eclipce', 1, 2),
(3, 'brakets', 1, 2),
(4, 'bluej', 1, 2),
(5, 'oracle', 1, 2),
(6, 'visual studio', 0, 2),
(7, 'falso', 0, 3),
(8, 'objeto', 0, 4),
(9, 'objeto', 0, 5),
(25, 'public', NULL, 10),
(26, 'private', NULL, 10),
(27, 'protected', NULL, 10),
(28, 'static', NULL, 10),
(29, 'clases adstractas', NULL, 10),
(30, 'falso', 0, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pregunta`
--

CREATE TABLE `tipo_pregunta` (
  `id_tipo_pregunta` int(11) NOT NULL,
  `categoria` varchar(50) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_pregunta`
--

INSERT INTO `tipo_pregunta` (`id_tipo_pregunta`, `categoria`) VALUES
(1, 'seleccion_multiple'),
(2, 'falso_verdadero'),
(3, 'rellenar_espacios');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id_curso`);

--
-- Indices de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD PRIMARY KEY (`id_estudiante`);

--
-- Indices de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  ADD PRIMARY KEY (`id_pregunta`),
  ADD KEY `id_tipo_pregunta` (`id_tipo_pregunta`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `respuesta`
--
ALTER TABLE `respuesta`
  ADD PRIMARY KEY (`id_respuesta`),
  ADD KEY `id_pregunta` (`id_pregunta`);

--
-- Indices de la tabla `tipo_pregunta`
--
ALTER TABLE `tipo_pregunta`
  ADD PRIMARY KEY (`id_tipo_pregunta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  MODIFY `id_estudiante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  MODIFY `id_pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `respuesta`
--
ALTER TABLE `respuesta`
  MODIFY `id_respuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `tipo_pregunta`
--
ALTER TABLE `tipo_pregunta`
  MODIFY `id_tipo_pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pregunta`
--
ALTER TABLE `pregunta`
  ADD CONSTRAINT `pregunta_ibfk_1` FOREIGN KEY (`id_tipo_pregunta`) REFERENCES `tipo_pregunta` (`id_tipo_pregunta`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pregunta_ibfk_2` FOREIGN KEY (`id_curso`) REFERENCES `cursos` (`id_curso`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `respuesta`
--
ALTER TABLE `respuesta`
  ADD CONSTRAINT `respuesta_ibfk_1` FOREIGN KEY (`id_pregunta`) REFERENCES `pregunta` (`id_pregunta`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
