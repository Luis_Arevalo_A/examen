<?php require('conexion.php'); ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">    
    <title>Temario</title>    
    <link href="bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">    
    <link href="temario.css" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="#">Cursos</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="estudiante_pagina_principal.php">Inicio <span class="sr-only">(current)</span></a>
          </li>                              
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
        </form>
      </div>
    </nav>

    <main role="main" class="container">
        <section class="section">
                <div class="container">
                    <div class="row">					
                        <div class="col-md-10">
                            <!-- FILE STRUCTURE -->
                            <div class="pb-5 mb-5 border-bottom" id="file-structure">                                                                
                                <?php                                    
                                    $consulta_temario = mysqli_query($conexion,"SELECT * FROM cursos WHERE id_curso=".$_GET['id']);
                                    while ($f=mysqli_fetch_array($consulta_temario)) {                                                                                 
                                ?>
                                <!-- Heading -->
                                <h1 class="mb-4 color_blanco">
                                   <?php echo $f['nombre'];?>
                                </h1>
                                <div >
                                    <img src="<?php echo $f['imagen'];?>" alt="" class="imagen">
                                </div>
                                <br>
                                <!-- Content -->
                                <p class="lead parrafo">
                                    <?php echo $f['temario'];?>
                                </p>

                                <?php 
                                    }
                                ?>
                            </div>						
                        </div>
                    </div> <!-- / .row -->
                </div> <!-- / .container -->
        </section>             
        <p>
            <a href="#confirmacion_evaluacion" class="btn btn-primary my-2" data-toggle="modal" >Dar Evaluacion </a>                            
            <a href="#" class="btn btn-secondary my-2"> Nota</a>           
        </p>         
        <!-- Modal -->
                <div class="modal fade" id="confirmacion_evaluacion" tabindex="-1" role="dialog" aria-labelledby="confirmacion_evaluacionTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="confirmacion_evaluacionTitle">Confirmar Evaluacion</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <?php                                    
                                    $consulta_temario = mysqli_query($conexion,"SELECT * FROM cursos WHERE id_curso=".$_GET['id']);
                                    while ($f=mysqli_fetch_array($consulta_temario)) {                                                                                 
                                ?>
                                <p class="lead">Estas seguro de hacer la evaluacion!!!</p>
                                <!-- aca me falta poner el while para que busque en la base de datos -->
                                <p class="lead">El tiempo de la evaluacion es de <?php echo $f['tiempo']; ?> luego de eso no podras acceder</p>
                                <?php
                                        } ?>
                            </div>
                            <div class="modal-footer">                                
                                <a href="examen.php" class="btn btn-primary">Aceptar</a>                                
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>

    </main><!-- /.container -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
  </body>
</html>