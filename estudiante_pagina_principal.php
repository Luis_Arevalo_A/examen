<?php require('conexion.php'); ?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cursos</title>    
    <link href="bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">    
    <link href="album.css" rel="stylesheet">
</head>

<body>

    <header>
        <div class="navbar navbar-dark bg-dark box-shadow">
            <div class="container d-flex justify-content-between">
                <a href="#" class="navbar-brand d-flex align-items-center">
                    <strong>CURSOS Y EVALUACIONES</strong>
                </a>
            </div>
        </div>
    </header>

    <main role="main">

        <section class="jumbotron text-center">
            <div class="container">
                <h1 class="jumbotron-heading">Bienvenido a nuestros cursos online</h1>
                <p class="lead text-muted">Aca vas a poder tomar materias de tu interes sea cual sea tu carrea, lo que debes hacer es click en uno de
                    nuestros cursos y dar la evaluacion.</p>
            </div>
        </section>
    <!-- Esto agrega los cursos con la base de datos -->
        <div class="album py-5 bg-light">
            <div class="container">
                <div class="row">
            <?php
                while ($f=mysqli_fetch_array($res)) {                                    
            ?>                
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">                            
                            <img class="card-img-top" src="<?php echo $f['imagen']; ?>" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text"><?php echo$f['intro_curso'];?></p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">                                        
                                        <a href="ver_temario.php?id=<?php echo $f['id_curso'];?>" class="btn btn-sm btn-outline-secondary">Ver Temario</a>                                        
                                    </div>
                                    <small class="text-muted">Estudiantes registrados en el curso</small>
                                </div>
                            </div>
                        </div>
                    </div>

            <?php
                }
            ?>        
                </div>             
                 <!-- Termina de agregar -->
    </main>

    <footer class="text-muted">
        <div class="container">
            <p class="float-right">
                <a href="#">Back to top</a>
            </p>
            <p>Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
            <p>New to Bootstrap?
                <a href="../../">Visit the homepage</a> or read our
                <a href="../../getting-started/">getting started guide</a>.</p>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
    <script src="bootstrap-4.1.1/assets/js/vendor/holder.min.js"></script>
</body>

</html>