<?php require('conexion.php');?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>EVALUACION</title>
    <link href="bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="examen.css" rel="stylesheet">
    <script>    
        function carga(){
                        
            var contador_s=00;
            var contador_m=00;
            var contador_h=00;

            var s = document.getElementById("segundos");
            var m = document.getElementById("minutos");
            var h = document.getElementById("horas");

            window.setInterval(
                function(){
                    if (contador_s == 60) {
                        contador_s=0;
                        contador_m++;
                        if (contador_m == 60 ) {
                            contador_m=0;
                            contador_h++;
                            if (contador_h == 0) {
                                contador_h=0;
                            }
                        }
                    }
                    s.innerHTML = contador_s;
                    m.innerHTML = contador_m;
                    h.innerHTML = contador_h;
                    contador_s++;
                }
                ,1000);
        }
    </script>
</head>

<body onload="carga()">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <p class="navbar-brand">EVALUACION</p>
        <div class="collapse navbar-collapse" id="navbarCollapse">

           <div style="font-size: xx-large;color: white;border: solid;">
                <span id="horas">00</span>:<span id="minutos">00</span>:<span id="segundos">00</span>
           </div>
        </div>
    </nav>
<form action="enviar_evaluacion.php" method="POST">
    <main role="main" class="container">
        <!-- este es para la respuesta seleccion multiple -->
        <div class="my-3 p-3 bg-white rounded box-shadow">
            <?php                 
                $consultar_preguntas=mysqli_query($conexion,"SELECT * FROM pregunta
                where id_tipo_pregunta = 1");                                 
                $var=0;
                while ($f = mysqli_fetch_array($consultar_preguntas)) {                                    
                    $var=$f['id_pregunta'];                     
            ?>
            <div class="media text-muted pt-3">
                <p class="media-body pb-3 mb-0 small lh-125  border-gray">
                    <strong class="text-gray-dark tam"><?php echo $f['nombre_pregunta'];?></strong>
                </p>
            </div>
            <?php                 
                $consultar_respuestas=mysqli_query($conexion,"SELECT * FROM respuesta WHERE id_pregunta = '$var'"); 
                $fila2 = array();
                while ($fila = mysqli_fetch_array($consultar_respuestas)) {
                    array_push($fila2,$fila['respuesta_nombre']);
                }                                                 
            ?>            
            <div style="text-align: center; font-size: larger;" class="borde">
                <label>
                    <input type="radio" name="res1" value="1" class="radio"><?php echo $fila2[0];?>
                    <input type="radio" name="res1" value="0" style="visibility: hidden;" checked>
                </label>
                <label>
                    <input type="radio" name="res2" value="1" class="radio"><?php echo $fila2[1];?>
                    <input type="radio" name="res2" value="0" style="visibility: hidden;" checked>
                </label>
                <label>
                    <input type="radio" name="res3" value="1" class="radio"><?php echo $fila2[2];?>
                    <input type="radio" name="res3" value="0" style="visibility: hidden;" checked>
                </label>
                <label>
                    <input type="radio" name="res4" value="1" class="radio"><?php echo $fila2[3];?>
                    <input type="radio" name="res4" value="0" style="visibility: hidden;" checked>
                </label>
                <label>
                    <input type="radio" name="res5" value="1" class="radio"><?php echo $fila2[4];?>
                    <input type="radio" name="res5" value="0" style="visibility: hidden;" checked>
                </label>
            </div>            
            <?php                                       
                }            
            ?>
        </div>
        <!-- fin seleccion multiple -->
        <!-- este es para la respuesta verdadero falso -->
        <div class="my-3 p-3 bg-white rounded box-shadow">
            <?php 
                $consultar_preguntas_f=mysqli_query($conexion,"SELECT * FROM pregunta
                where id_tipo_pregunta = 2");   
                $incremento = 1;
                $opcion = "opcion".$incremento;
                while($f = mysqli_fetch_array($consultar_preguntas_f)){
            ?>
            <div class="media text-muted pt-3">
                <p class="media-body pb-3 mb-0 small lh-125  border-gray">
                    <strong class="text-gray-dark tam"><?php echo $f['nombre_pregunta'];?></strong>
                </p>
            </div>
                    
            <div style="text-align: center; font-size: larger;" class="borde">
                <label>                                  
                    <input type="radio" name='opcion<?php echo $incremento;?>' id="verdadero" value="1" class="radio" checked>Verdadero
                </label>
                <label>                    
                    <input type="radio" name='opcion<?php echo $incremento;?>' id="falso" value="0" class="radio">Falso
                </label>
            </div>
            <?php

            $incremento++;

            }
            ?>
        </div>
        <!-- fin verdadero falso -->
        <!-- este es para el rellenado de espacios -->
        <div class="my-3 p-3 bg-white rounded box-shadow">
            <?php 
            $consultar_preguntas_r=mysqli_query($conexion,"SELECT * FROM pregunta
            where id_tipo_pregunta = 3");   
            while ($f = mysqli_fetch_array($consultar_preguntas_r)) {
                
            ?>
            <div class="media text-muted pt-3">
                <p class="media-body pb-3 mb-0 small lh-125  border-gray">
                    <strong class="text-gray-dark tam"><?php echo $f['nombre_pregunta'];?></strong>
                </p>
            </div>            
            <div style="text-align: center; font-size: larger;" class="borde">
               <textarea name="res_llenado"placeholder="Separar la respuesta con una coma, sino la respuesta se lo tomara como mala. Ejm: frutas,verduras" cols="100" rows="5"></textarea>
            </div>
            <?php
            }
            ?>
        </div>
        <!-- fin rellenado espacios -->
    </main>
    <div style="text-align: center">
            <input type="submit" value="Guardar" class="btn btn-success" style="font-size: xx-large">
    </div>
    
</form>
    

    <!-- <nav>
        <ul class="pagination justify-content-center">
            <li class="page-item disabled">
                <a class="page-link" href="#">Anterior</a>
            </li>
            <li class="page-item active">
                <a class="page-link" href="#">1</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">3</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">Siguiente</a>
            </li>
        </ul>
    </nav> -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
</body>

</html>